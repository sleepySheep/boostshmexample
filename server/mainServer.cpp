#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <cstdlib>
#include <cstring>
#include <iostream>

#define SHM_SIZE 100

using namespace boost::interprocess;

int main()
{
    std::cout << "Hello Server" << std::endl;

    // Create shared mameory object and map to region
    shared_memory_object shm_obj(create_only, "shared_memory_obj", read_write);
    shm_obj.truncate(SHM_SIZE * sizeof(int));
    mapped_region region(shm_obj, read_write);

    std::cout << "shared memory ptr is: " << region.get_address() << std::endl;

    // Define test array
    int testArray[SHM_SIZE];
    for(int i = 0; i < SHM_SIZE; i++) testArray[i] = i;

    // Copy contents from test array to shared memory
    std::memcpy(region.get_address(), testArray, sizeof(testArray));

    // Test if contents correctly written
    int *mem = static_cast<int *>(region.get_address());
    for(std::size_t i = 0; i < SHM_SIZE; ++i)
        std::cout << i << ": " << static_cast<int>(*mem++) << std::endl;

    // Exit control
    std::string str("");
    while(str != "exit")
    {
        std::cout << "Type \"exit\" to continue" << std::endl;
        std::cin >> str;
        std::cout << "Exiting example" << std::endl;
    }

    // Tell OS to release memory region
    shared_memory_object::remove("shared_memory_obj");

    return 0;
}
