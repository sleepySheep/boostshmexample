#include <boost/interprocess/mapped_region.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <iostream>
#include <string>

#define SHM_SIZE 100

using namespace boost::interprocess;

int main()
{
    std::cout << "Hello Client" << std::endl;

    shared_memory_object shm_obj(open_only, "shared_memory_obj", read_only);
    mapped_region region(shm_obj, read_only);

    std::cout << "shared memory ptr is: " << region.get_address() << std::endl;

    int *mem = static_cast<int *>(region.get_address());
    for(std::size_t i = 0; i < SHM_SIZE; ++i)
        std::cout << i << ": " << static_cast<int>(*mem++) << std::endl;

    return 0;
}
